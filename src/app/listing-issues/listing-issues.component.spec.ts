import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingIssuesComponent } from './listing-issues.component';

describe('ListingIssuesComponent', () => {
  let component: ListingIssuesComponent;
  let fixture: ComponentFixture<ListingIssuesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListingIssuesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
