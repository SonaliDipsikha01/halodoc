import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-listing-issues',
  templateUrl: './listing-issues.component.html',
  styleUrls: ['./listing-issues.component.less']
})
export class ListingIssuesComponent implements OnInit {
  pageEvent: PageEvent;
  allIssue: any;
  searchQuery: string;
  filtersLoaded: Promise<boolean>;

  length: any = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageNo: number;

  constructor(private service: DataService) { }

  ngOnInit(): void {

    this.pageChange(null)

  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }
  search(event: any) {
    this.searchQuery = event.target.value;
    this.pageChange(null)

  }
  pageChange(event: any) {
    this.pageNo = event && event.pageIndex ? event.pageIndex + 1 : 1;

    this.service.getPaginationData(this.pageNo, this.searchQuery).subscribe(
      response => {
        this.allIssue = response;
        this.length = this.allIssue.total_count;
        this.filtersLoaded = Promise.resolve(true); 
      },
      error => {
      }
    );
    return event;
  }
}
