import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'halodoc';
  repoDetails:any;
  checkIfPublicRepo:string='';
  filtersLoaded: Promise<boolean>;
  constructor(private service:DataService){

  }
  subscribe=new Subscription();
  ngOnInit(): void {
    this.getReposDetails()
  }

  getReposDetails(){
    this.service.getRepositoryDetails().subscribe((res:any) => {
      this.repoDetails=res;
      this.ifPublicRepo(res.private);
      this.filtersLoaded = Promise.resolve(true); 
        },
        (err:any)=>{
        });
  }

  ifPublicRepo(value:any){
    this.checkIfPublicRepo=value==false? 'Public':'Private'
  }
 
}
