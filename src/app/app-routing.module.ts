import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IssueDetailsComponent } from './issue-details/issue-details.component';
import { ListingIssuesComponent } from './listing-issues/listing-issues.component';
const routes: Routes = [
  {
    path:'details/:id',component:IssueDetailsComponent,
   
  },
{
  path:'issues',component:ListingIssuesComponent
}
,
{
  path:'**',redirectTo:'issues',
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
