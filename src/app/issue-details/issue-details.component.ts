import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-issue-details',
  templateUrl: './issue-details.component.html',
  styleUrls: ['./issue-details.component.less']
})
export class IssueDetailsComponent implements OnInit {
  private sub: Subscription; 
  issueId:number;
  issueDetails:any;
  filtersLoaded: Promise<boolean>;

  constructor(private route: ActivatedRoute,private service:DataService) { }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.issueId=params['id']
    });
    if(this.issueId)
    this.getIssueDetails()
  }


  getIssueDetails(){
    this.service.getIssueDetails(this.issueId).subscribe((res)=>{
      this.issueDetails=res;
      this.filtersLoaded = Promise.resolve(true); 

    },
    (err)=>{
        
    }
    )
  }



  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
