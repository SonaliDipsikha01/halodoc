import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  getRepositoryDetails(){
   return this.http.get('https://api.github.com/repos/angular/angular')
  }

  getPaginationData(pageNo:any,que:any){
    var query;
    if(que)
      query={params:{q:que}}
    
      
    return this.http.get(`https://api.github.com/search/issues?q=repo:angular/angular/node+type:issue+state:open&per_page=10&page=${pageNo}`,query)
  }
  getIssueDetails(issueId:number){
   return  this.http.get(`https://api.github.com/repos/angular/angular/issues/${issueId}`)
  }

  
}
